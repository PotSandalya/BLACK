// Подключение графической библиотеки
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

int main()
{
    // Создание окна с известными размерами и названием
    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML works!");
    
    

    // Прямоугольники
    int x1, x2, x3, y1, y2, y3;
    x1 = 780;
    x2 = 710;
    x3 = 740;
    y1 = 570;
    y2 = 370;
    y3 = 100;
    sf::RectangleShape rectangle_1(sf::Vector2f(10, 20));
    rectangle_1.setFillColor(sf::Color::Red);
    rectangle_1.setPosition(x1, y1);

    sf::RectangleShape rectangle_2(sf::Vector2f(80, 100));
    rectangle_2.setFillColor(sf::Color::Blue);
    rectangle_2.setPosition(x2, y2);

    sf::RectangleShape rectangle_3(sf::Vector2f(50, 30));
    rectangle_3.setFillColor(sf::Color::Green);
    rectangle_3.setPosition(x3, y3);

    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                // окно закрывается
                window.close();
        }


        //перемеение прямоугольников
        x1 = x1 - 8;
        x2 = x2 - 4;
        x3 = x3 - 6;

        if (x1 < 10)
            x1 = 10;
        if (x2 < 10)
            x2 = 10;
        if (x3 < 10)
            x3 = 10;
       
        rectangle_1.setPosition(x1, y1);
        rectangle_2.setPosition(x2, y2);
        rectangle_3.setPosition(x3, y3);
        
       
        // Очистить окно от всего
        window.clear();

        // Перемещение фигуры в буфер
        window.draw(rectangle_1);
        window.draw(rectangle_2);
        window.draw(rectangle_3);

        // Отобразить на окне все, что есть в буфере
        window.display();

        std::this_thread::sleep_for(50ms);
    }

    return 0;
}
